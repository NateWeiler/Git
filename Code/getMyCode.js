// Run this with node and then run the output with sh
let Http = require('http')
let ChildProcess = require('child_process');
Http.cat("http://github.com/api/v2/json/repos/show/creationix", function (err, json) {
  if (err) throw err;
  var data = JSON.parse(json);
  data.repositories.forEach(function (repo) {
    console.log("echo " + JSON.stringify(repo.name + " - " + repo.description));
    console.log("git clone --bare " + repo.url);
  });
});
