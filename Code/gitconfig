[user]
  name = Nathan Weiler
  email = nateweiler84@gmail.com
[core]
  editor = code
[push]
  default = simple
[github]
  user = NateWeiler
  password = 
  oauth-token =
[credential]
  helper = osxkeychain
[alias]
  # Adding
  #---------
  # shortcut
  a = add
  # add all
  aa = add -A
  # add interactively
  ai = add -i

  # Archiving
  # -----------
  # tag a branch as archive and delete it
  archive = "!f() { git tag archive/"$1" "$1" && git bd "$1";}; f"

  # Branching
  # -----------
  # show branches
  b = branch
  # show all branches
  ba = branch -a
  # delete branch
  bd = branch -d
  # delete unmerged branch
  bdd = branch -D
  # rename a branch to <branch>-rebased
  brr = "!f() { git branch | grep "$1" | cut -c 3- | grep -v rebased | xargs -I{} git branch -m {} {}-rebased; }; f"
  # remove local branches that have been merged 
  # trim = "!f() { git branch --merged ${1-dev} | grep -v " ${1-dev}$" | xargs git branch -d; }; f"

  # Checkout
  #-----------
  # shortcut
  co = checkout
  # new branch
  cob = checkout -b

  # Commit
  #---------
  # shortcut
  c = commit
  # commit with message
  cm = commit -m "Add"
  # commit all modified/deleted
  ca = commit -a -m
  # commit all modified/deleted with message
  cam = commit -a -m "Add"

  # Config
  #---------
  # edit config file
  cfge = config -e
  # show config file
  cfgl = config -l
  # show aliases
  cfga = "!git config -l | grep alias | cut -c 7-"

  # Grepping
  #-----------
  g = grep -Ii

  # Logging
  #----------
  # simple list of commits
  l = log --oneline --decorate
  # list commits with color-coded branch and tag annotations
  lc = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate
  # list commits and files changed with color-coded branch and tag annotations
  ll = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --numstat
  # graph commits and files changed with color-coded branch and tag annotations
  lg = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --graph
  # list commits in short form without colors to pipe to other commands
  lnc = log --pretty=format:"%h\\ %s\\ [%cn]"
  # list commits with dates
  lcd = log --pretty=format:"%C(yellow)%h\\ %ad%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --date=short
  # list commits with relative dates
  lcr = log --pretty=format:"%C(yellow)%h\\ %ad%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --date=relative
  # list commits relating to a single file
  lf = log -u

  # show new commits after a fetch
  new = ls ../origin/master

  # Status
  #----------
  # shortcut
  s = status
  # short format
  ss = status -s

  # Submodule
  #------------
  # initialize submodules
  subin = submodule init
  # update submodules
  subup = submodule update --init --recursive

[filter "lfs"]
	clean = git-lfs clean -- %f
	smudge = git-lfs smudge -- %f
	process = git-lfs filter-process
	required = true
